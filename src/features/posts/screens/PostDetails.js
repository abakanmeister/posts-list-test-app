import React from 'react';
import { View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import styles from '../styles';
import propTypes from 'prop-types';

import MyText from '../../../components/MyText';

class PostDetails extends React.Component {
    static navigationOptions = ({navigation}) => ({
        title: 'title' in navigation.state.params ? navigation.state.params.title : 'Post Details',
    });

    renderItem = ({item, index}) => {
        return (
            <View style={[styles.commentsCard, index === 0 && {marginTop: 10}]}>
                <MyText style={styles.userName}>{item.name}</MyText>
                <MyText style={styles.userComment}>{item.body}</MyText>
                <MyText style={styles.userEmail}>{item.email}</MyText>
            </View>
        )
    };

    render() {
        const { comments } = this.props;
        return (
            <View style={styles.container}>
                <FlatList data={comments}
                          renderItem={this.renderItem}
                          keyExtractor={(item, index) => `list-item-${index}`} />
            </View>
        );
    }
}
const mapStateToProps = state => ({
    comments: state.comments.items
});
export default connect(mapStateToProps)(PostDetails);

PostDetails.propTypes = {
    comments: propTypes.array.isRequired
};
