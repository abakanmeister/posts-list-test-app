import React from 'react';
import { FlatList, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import styles from '../styles';
import { fetchPosts, selectPost, fetchComments } from '../../posts/actions';
import Spinner from 'react-native-loading-spinner-overlay';
import propTypes from 'prop-types';
import MyText from '../../../components/MyText';

class PostsList extends React.Component {
    showPostDetails = post => {
        const { selectPost, navigation, fetchComments } = this.props,
            { title } = post;
        selectPost(post.id).then(() => {
            fetchComments(post.id)
                .then(() => navigation.navigate('PostDetails', {title}));
        })
    };
    componentDidMount() {
        this.props.fetchPosts();
    }
    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity style={[styles.commentsCard, index === 0 && {marginTop: 10}]}
                              onPress={() => this.showPostDetails(item)}>
                <MyText style={styles.postTitle}>{item.title}</MyText>
                <MyText style={styles.postBody}>{item.body}</MyText>
            </TouchableOpacity>
        )
    };
    render() {
        const { fetchFailed, isFetching, posts } = this.props;
        return (
            <View style={styles.container}>
                <Spinner visible={!fetchFailed && isFetching} textContent={'loading...'}/>
                {!fetchFailed &&
                    <View style={styles.margins}>
                        <FlatList data={posts}
                                  renderItem={this.renderItem}
                                  keyExtractor={(item, index) => `list-item-${index}`}/>
                    </View>
                }
                {fetchFailed &&
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <MyText style={{textAlign: 'center'}}>Oops. Something goes wrong. Restart the app, please ;)</MyText>
                    </View>
                }
            </View>
        );
    }
}
const mapStateToProps = state => ({
    isFetching: state.posts.isFetching || state.comments.isFetching,
    fetchFailed: state.posts.fetchFailed || state.comments.fetchFailed,
    posts: state.posts.items
});
export default connect(mapStateToProps, {fetchPosts, fetchComments, selectPost})(PostsList);

PostsList.propTypes = {
    isFetching: propTypes.bool.isRequired,
    fetchFailed: propTypes.bool.isRequired,
    posts: propTypes.array.isRequired
};
