import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';
import {SSP_SemiBold} from '../../../styles/fonts';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background.blue
    },
    commentsCard: {
            flex: 1,
            backgroundColor: colors.lightgray,
            borderRadius: 5,
            padding: 5,
            marginBottom: 15,
            elevation: 5,
            marginHorizontal: 10
    },
    margins: {
        margin: 5
    },
    detailsTitle: {

    },
    detailsBody: {

    },
    postTitle: {
        marginBottom: 10,
        fontSize: 18,
        fontFamily: SSP_SemiBold,
        paddingBottom: 3,
        borderBottomWidth: 1,
        borderColor: colors.black
    },
    postBody: {

    },
    userName: {
        fontSize: 16
    },
    userEmail: {
        fontSize: 16,
        textAlign: 'right',
        fontStyle: 'italic'
    },
    userComment: {
        marginVertical: 5
    }
})
