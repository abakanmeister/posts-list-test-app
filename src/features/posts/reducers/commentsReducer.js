import { COMMENTS_FETCHING, COMMENTS_FETCHING_FAILED, COMMENTS_FETCHED } from '../actions/types';

const initialState = {
    items: [],
    isFetching: false,
    fetchFailed: false
};

export default (state = initialState, action) => {
    const { payload:income } = action;
    switch (action.type) {
        case COMMENTS_FETCHING: {
            return {
                ...state,
                isFetching: true,
                fetchFailed: false
            };
        }
        case COMMENTS_FETCHING_FAILED: {
            return {
                ...state,
                isFetching: false,
                fetchFailed: true
            };
        }
        case COMMENTS_FETCHED: {
            return {
                ...state,
                isFetching: false,
                fetchFailed: false,
                items: income
            }
        }
        default:
            return state;
    }
};
