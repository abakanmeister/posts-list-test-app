import { POSTS_FETCHING, POSTS_FETCHING_FAILED, POSTS_FETCHED } from '../actions/types';

const initialState = {
    items: [],
    isFetching: false,
    fetchFailed: false
};

export default (state = initialState, action) => {
    const { payload:income } = action;
    switch (action.type) {
        case POSTS_FETCHING: {
            return {
                ...state,
                isFetching: true,
                fetchFailed: false
            };
        }
        case POSTS_FETCHING_FAILED: {
            return {
                ...state,
                isFetching: false,
                fetchFailed: true
            };
        }
        case POSTS_FETCHED: {
            return {
                ...state,
                isFetching: false,
                fetchFailed: false,
                items: income
            }
        }
        default:
            return state;
    }
};
