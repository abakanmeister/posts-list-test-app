export const POSTS_FETCHING = 'POSTS_FETCHING';
export const POSTS_FETCHING_FAILED = 'POSTS_FETCHING_FAILED';
export const POSTS_FETCHED = 'POSTS_FETCHED';
export const SELECT_POST = 'SELECT_POST';
export const COMMENTS_FETCHING = 'COMMENTS_FETCHING';
export const COMMENTS_FETCHING_FAILED = 'COMMENTS_FETCHING_FAILED';
export const COMMENTS_FETCHED = 'COMMENTS_FETCHED';
