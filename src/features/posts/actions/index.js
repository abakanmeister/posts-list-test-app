import * as types from './types';
import { postsSourceUrl, commentsSourceUrl } from '../../../api';

export const fetchPosts = () => dispatch => {
    dispatch({
        type: types.POSTS_FETCHING,
        payload: {}
    });
    fetch(postsSourceUrl)
        .then(posts => posts.json())
        .then(posts => {
            dispatch({
                type: types.POSTS_FETCHED,
                payload: posts
            })
        })
        .catch(e => {
            dispatch({
                type: types.POSTS_FETCHING_FAILED,
                payload: {}
            })
        })
};

export const selectPost = id => dispatch => new Promise(resolve => {
    dispatch({
        type: types.SELECT_POST,
        payload: {}
    });
    resolve();
});

export const fetchComments = postId => dispatch => {
    dispatch({
        type: types.COMMENTS_FETCHING
    });
    return fetch(commentsSourceUrl(postId))
        .then(comments => comments.json())
        .then(comments => {
            dispatch({
                type: types.COMMENTS_FETCHED,
                payload: comments
            })
        })
        .catch(e => {
            dispatch({
                type: types.COMMENTS_FETCHING_FAILED,
                payload: {}
            })
        })
};
