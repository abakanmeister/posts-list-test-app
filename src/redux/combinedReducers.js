import { combineReducers } from 'redux';
import postsReducer from '../features/posts/reducers/postsReducer';
import commentsReducer from '../features/posts/reducers/commentsReducer';

const appReducer = combineReducers({
    posts: postsReducer,
    comments: commentsReducer
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

export default rootReducer;
