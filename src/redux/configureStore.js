import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist';
import combinedReducers from '../redux/combinedReducers';

const middleware = [thunk],
    initialState = {},
    composeEnhancers = compose,
    persistConfig = {
        key: 'root',
        storage: AsyncStorage
    };
const persistedReducer = persistReducer(persistConfig, combinedReducers);
const configureStore = createStore(
    persistedReducer,
    initialState,
    composeEnhancers (
        applyMiddleware(...middleware)
    )
);

export default configureStore;
export const persistor = persistStore(configureStore);
