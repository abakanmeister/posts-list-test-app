import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import colors from '../styles/colors';

export default class MyTextInput extends React.Component{
    state = {
        isFocused: false
    };

    handleFocus = event => {
        this.setState({ isFocused: false });
        this.props.onFocus && this.props.onFocus(event);
    };

    handleBlur = event => {
        this.setState({ isFocused: false });
        this.props.onBlur && this.props.onBlur(event);
    };

    render() {
        const { isFocused } = this.state,
            { onFocus, onBlur, placeholder, ...otherProps} = this.props;
        return (
            <TextInput
                placeholder={placeholder}
                selectionColor={colors.red}
                underlineColorAndroid={
                    isFocused ? 'lightgray' : colors.red
                }
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                style={styles.textInput}
                {...otherProps}
            />
        );
    }
}
const styles = StyleSheet.create({
    textInput: {
        height: 40,
        paddingLeft: 6
    }
});
