import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';

import MyText from './MyText';
import colors from '../styles/colors';

export default props => (
    <TouchableOpacity style={[styles.wrapper, props.style]}
                      disabled={props.disabled}
                      onPress={props.onPress}>
        <MyText style={styles.title}>{props.title}</MyText>
    </TouchableOpacity>)

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.lightgray,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        paddingHorizontal: 10
    },
    title: {
        color: '#fff',
        textTransform: 'uppercase',
        fontSize: 16
    }
});
