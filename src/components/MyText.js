import React from 'react';
import {Text} from 'react-native';
import {SSP_Regular} from '../styles/fonts';
import colors from '../styles/colors';
export default props => <Text {...props}
                              style={[{fontFamily: SSP_Regular, fontSize: 16, color: colors.black}, props.style]}>{props.children}</Text>
