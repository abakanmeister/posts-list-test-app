export default {
    black: '#232323',
    background: {
        blue: 'rgba(127, 229, 240, 0.1)'
    },
    lightgray: 'lightgray'
};
