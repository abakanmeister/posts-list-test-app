import React from 'react';
import {
    SafeAreaView,
} from 'react-native';
import AppNavigator from './navigation';

console.disableYellowBox = true;
export default class App extends React.Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <AppNavigator />
            </SafeAreaView>
        );
    }
}

