import React from 'react';
import { createStackNavigator, createAppContainer, } from "react-navigation";
import PostsListScreen from '../features/posts/screens/PostsList';
import PostDetailsScreen from '../features/posts/screens/PostDetails';

const stackNavigator = createStackNavigator({
    PostsList: {
        screen: PostsListScreen,
        navigationOptions: () => ({
            title: 'Posts'
        })
    },
    PostDetails: {
        screen: PostDetailsScreen,
    },
}, {
    initialRouteName: 'PostsList',
    navigationOptions: {
        mode: 'modal'
    }
});

export default createAppContainer(stackNavigator);
