export const postsSourceUrl = 'https://jsonplaceholder.typicode.com/posts';
export const commentsSourceUrl = postId => `https://jsonplaceholder.typicode.com/posts/${postId}/comments`;
